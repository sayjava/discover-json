import { Datasource, FindModel } from "discover";
import { readFileSync } from "fs";
import sift from "sift";

export interface JSONConfig {
  file: string;
  persist?: boolean;
}

// map the source names to the field names
const mapField = (row: any, model: FindModel): any => {
  return Object.keys(model.attributes).reduce((acc: any, attrKey: string) => {
    const attr = model.attributes[attrKey];

    return Object.assign(acc, row, {
      [attr.name]: row[attr.sourceName]
    });
  }, {});
};

const skipAndLimit = (rows: any[], model: FindModel): any[] => {
  const skip = model.criteria.skip ? model.criteria.skip : 0;
  const limit = model.criteria.limit
    ? skip + model.criteria.limit
    : rows.length;

  return rows.slice(skip, limit);
};

// recusively apply models
const applyFilter = (rows: any[], model: FindModel) => {
  const baseFilters: any = model.criteria.$and || model.criteria.$or || {};
  const filterFunc = sift(baseFilters);
  const filtered = rows.filter(filterFunc);

  Object.keys(model.relations).forEach(relationName => {
    const relationship = model.relations[relationName];
    const childModel = <FindModel>relationship.model;

    filtered.forEach((filteredRow: any) => {
      const children: any[] = filteredRow[relationName] || [];
      filteredRow[relationName] = applyFilter(children, childModel);
    });
  });

  return skipAndLimit(filtered, model).map(row => mapField(row, model));
};

export default (config: JSONConfig): Datasource => {
  const rows: any[] = JSON.parse(readFileSync(config.file).toString());

  return {
    find: (model: FindModel) => {
      const filtered = applyFilter(rows, model);
      return Promise.resolve(filtered);
    }
  };
};
