import source from "../lib";
import { discover, APIConfig } from "discover";
import { graphql } from "graphql";

const typeDef = `
    type User {
        id: Int @unique
        firstName:String @field(name:"first_name")
        lastName: String @field(name:"last_name")
        tasks: [Task!]
    }

    type Task {
        id: ID
        name: String
        owner: User
    }
`;

test("Query: Limit & Skip", async () => {
  const sources = {
    default: source({ file: "./test/fixtures/user_tasks.json" })
  };

  const config: APIConfig = {
    typeDef,
    sources
  };

  const schema = discover(config);

  const query = `
        query {
            find_users(skip: 2, limit: 2) {
                firstName
            }
        }
      `;

  const result = await graphql(schema, query);

  expect(result.errors).toBeUndefined();
  expect(result.data).toMatchInlineSnapshot(`
    Object {
      "find_users": Array [
        Object {
          "firstName": "Stacey",
        },
        Object {
          "firstName": "Keri",
        },
      ],
    }
  `);
});

test("Query: equal filter", async () => {
  const sources = {
    default: source({ file: "./test/fixtures/simple_tasks.json" })
  };

  const config: APIConfig = {
    typeDef,
    sources
  };

  const schema = discover(config);

  const query = `
        query {
          find_users(where: { firstName: { _eq: "Stacey" } }) {
              firstName
              lastName

              tasks {
                  name
              }
          }
        }
      `;

  const result = await graphql(schema, query);

  expect(result.errors).toBeUndefined();

  expect(result.data).toMatchInlineSnapshot(`
    Object {
      "find_users": Array [
        Object {
          "firstName": "Stacey",
          "lastName": "Eingerfield",
          "tasks": Array [
            Object {
              "name": "web-enabled",
            },
          ],
        },
      ],
    }
  `);
});

test("Query: relationship equal filter", async () => {
  const sources = {
    default: source({ file: "./test/fixtures/simple_tasks.json" })
  };

  const config: APIConfig = {
    typeDef,
    sources
  };

  const schema = discover(config);

  const query = `
        query {
          find_users(where: { firstName: { _eq: "Keri" } }) {
              firstName
              lastName

              tasks(where: { name: { _eq: "web-enabled" }}) {
                  name
              }
          }
        }
      `;

  const result = await graphql(schema, query);

  expect(result.errors).toBeUndefined();
  expect(result.data).toMatchInlineSnapshot(`
Object {
  "find_users": Array [
    Object {
      "firstName": "Keri",
      "lastName": "Skippen",
      "tasks": Array [
        Object {
          "name": "web-enabled",
        },
      ],
    },
  ],
}
`);
});

test("Query: relationship and paging", async () => {
  const sources = {
    default: source({ file: "./test/fixtures/simple_tasks.json" })
  };

  const config: APIConfig = {
    typeDef,
    sources
  };

  const schema = discover(config);

  const query = `
        query {
          find_users(where: { firstName: { _eq: "Keri" } }) {
              firstName
              lastName

              tasks(limit: 1, skip: 1) {
                  name
              }
          }
        }
      `;

  const result = await graphql(schema, query);

  expect(result.errors).toBeUndefined();
  expect(result.data).toMatchInlineSnapshot(`
Object {
  "find_users": Array [
    Object {
      "firstName": "Keri",
      "lastName": "Skippen",
      "tasks": Array [
        Object {
          "name": "data-warehouse",
        },
      ],
    },
  ],
}
`);
});

test("Query: relation and all children", async () => {
  const sources = {
    default: source({ file: "./test/fixtures/simple_tasks.json" })
  };

  const config: APIConfig = {
    typeDef,
    sources
  };

  const schema = discover(config);

  const query = `
        query {
          find_users(where: { firstName: { _eq: "Keri" } }) {
              firstName
              lastName

              tasks {
                  name
              }
          }
        }
      `;

  const result = await graphql(schema, query);

  expect(result.errors).toBeUndefined();
  expect(result.data).toMatchInlineSnapshot(`
Object {
  "find_users": Array [
    Object {
      "firstName": "Keri",
      "lastName": "Skippen",
      "tasks": Array [
        Object {
          "name": "web-enabled",
        },
        Object {
          "name": "data-warehouse",
        },
        Object {
          "name": "service-desk",
        },
        Object {
          "name": "systematic",
        },
      ],
    },
  ],
}
`);
});
